// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyBKGOSigf4JtleYyaNYnXM_fG2uYEzhN2Y",
    authDomain: "users-39498.firebaseapp.com",
    databaseURL: "https://users-39498.firebaseio.com",
    projectId: "users-39498",
    storageBucket: "users-39498.appspot.com",
    messagingSenderId: "440491814482"
  }
};