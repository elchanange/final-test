import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
//Fire Base
import { AngularFireDatabase } from 'angularfire2/database';

//Deployment
import { environment } from './../../environments/environment';


@Injectable()
export class UsersService {
  http:Http;
    constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }
  getUsers(){
    return this.http.get(environment.url + 'users');
  }
  //Post


  postUsers(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);
    return this.http.post(environment.url + 'users', params.toString(), options);
  }
//Delete
deleteUser(key){
  return this.http.delete(environment.url + 'users/'+ key);
}
//Fire Base
  getUsersFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/users').valueChanges();
  }

 //Get one user
  getUser(id){
     return this.http.get(environment.url + 'users/'+ id);
  }


//Put
putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('name',data.name).append('phone',data.phone);
 return this.http.put(environment.url + 'users/'+ key,params.toString(), options);
}

}












/*import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
//Fire Base
import { AngularFireDatabase } from 'angularfire2/database';

//Deployment
import { environment } from './../../environments/environment';


@Injectable()
export class UsersService {
  http:Http;
    constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }
  getUsers(){
    return this.http.get('http://localhost/angular/slim/users');
  }
  //Post


  postUsers(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);
    return this.http.post('http://localhost/angular/slim/users', params.toString(), options);
  }
//Delete
deleteUser(key){
  return this.http.delete('http://localhost/angular/slim/users/'+ key);
}
//Fire Base
  getUsersFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/users').valueChanges();
  }

 //Get one user
  getUser(id){
     return this.http.get('http://localhost/angular/slim/users/'+ id);
  }


//Put
putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('name',data.name).append('phone',data.phone);
 return this.http.put('http://localhost/angular/slim/users/'+ key,params.toString(), options);
}

}
*/